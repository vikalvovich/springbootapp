package by.softclub.springbootapp.controller;

import by.softclub.springbootapp.dto.SimpleObjectDto;
import by.softclub.springbootapp.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello, Spring Boot!";
    }

    @PostMapping(value = "/hello2")
    public SimpleObjectDto hello2(@RequestBody SimpleObjectDto dto) {
        dto.setId(dto.getId() + 1);
        dto.setName("Hello " + dto.getName());
        return dto;
    }

    @GetMapping(value = "/hello3")
    public SimpleObjectDto hello3(@RequestParam Integer id,
                                  @RequestParam String name) {
        SimpleObjectDto dto = new SimpleObjectDto();
        dto.setId(id);
        dto.setName(name);
        return dto;
    }

    @GetMapping(value = "/hello4/{id}")
    public SimpleObjectDto hello4(@PathVariable Integer id) {
        SimpleObjectDto dto = new SimpleObjectDto();
        dto.setId(id);
        return dto;
    }

    @GetMapping(value = "/hello5")
    public List<SimpleObjectDto> hello5() {
        return testService.getList();
    }

    @GetMapping(value = "/data")
    public List<SimpleObjectDto> data(@RequestParam Integer page,
                                      @RequestParam Integer size) {
        return testService.getData(page, size);
    }

    @GetMapping(value = "/data/{type}")
    public List<SimpleObjectDto> sortedData(@PathVariable String type) {
        return testService.getSortedData(type);
    }

}
