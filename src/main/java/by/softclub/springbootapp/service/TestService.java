package by.softclub.springbootapp.service;

import by.softclub.springbootapp.dto.SimpleObjectDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface TestService {
    List<SimpleObjectDto> getList();

    List<SimpleObjectDto> getData(Integer page, Integer size);

    List<SimpleObjectDto> getSortedData(String fieldName);
}
