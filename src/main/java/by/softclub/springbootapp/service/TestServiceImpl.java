package by.softclub.springbootapp.service;

import by.softclub.springbootapp.comparator.IdComparator;
import by.softclub.springbootapp.comparator.NameComparator;
import by.softclub.springbootapp.dto.SimpleObjectDto;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.Math.min;
import static java.util.stream.Collectors.toMap;

@Service
public class TestServiceImpl implements TestService {

    private List<SimpleObjectDto> objects = new ArrayList<>();

    @PostConstruct
    public void init() {
        objects.add(SimpleObjectDto.builder().id(3).name("Bim").build());
        objects.add(SimpleObjectDto.builder().id(4).name("Mim").build());
        objects.add(SimpleObjectDto.builder().id(1).name("Tom").build());
        objects.add(SimpleObjectDto.builder().id(2).name("Tim").build());
        objects.add(SimpleObjectDto.builder().id(5).name("Kim").build());
    }

    @Override
    public List<SimpleObjectDto> getList() {
        return this.objects;
    }

    @Override
    public List<SimpleObjectDto> getData(Integer page, Integer size) {
        List<SimpleObjectDto> result = new ArrayList<>();
        Integer start = (page - 1) * size;
        if (start < this.objects.size()) {
            result = this.objects.subList(start, min(start + size, this.objects.size()));
        }
        return result;
    }

    @Override
    public List<SimpleObjectDto> getSortedData(String fieldName) {
        List<SimpleObjectDto> result = new ArrayList<>();
        if(fieldName.toUpperCase().equals("NAME")) {
            Collections.sort(this.objects, new NameComparator());
        } else {
            Collections.sort(this.objects, new IdComparator());
        }
        return this.objects;
    }


}
