package by.softclub.springbootapp.comparator;

import by.softclub.springbootapp.dto.SimpleObjectDto;

import java.util.Comparator;

public class NameComparator implements Comparator<SimpleObjectDto> {
    @Override
    public int compare(SimpleObjectDto o1, SimpleObjectDto o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
