package by.softclub.springbootapp.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SimpleObjectDto {
    private Integer id;
    private String name;
    private BigDecimal amount;

}
