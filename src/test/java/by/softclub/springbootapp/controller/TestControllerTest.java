package by.softclub.springbootapp.controller;

import by.softclub.springbootapp.service.TestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    TestService testService;

    @Test
    void sortedData() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/data/id") //несуществующий url
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        assertEquals(objectMapper.writeValueAsString(testService.getSortedData("id")), resultStr);
    }

    @Test
    void test404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/sortedData") //несуществующий url
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}